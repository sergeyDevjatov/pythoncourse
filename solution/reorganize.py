import argparse
import os
import time
from datetime import timedelta
from shutil import copyfile

def check_arg():
    ''' Parsing args '''

    parser = argparse.ArgumentParser(
        description='Reorganize files in some directory.')

    parser.add_argument('-s', '--source',
                        help='dir must be reorganized',
                        metavar="<path>",
                        default='.')
    parser.add_argument('-D', '--days',
                        help="""files was changed earlier than %(metavar)s
                                are put into the 'Archive'""",
                        type=int,
                        required=True,
                        metavar="<days>",
                        default='30')
    parser.add_argument('-S', '--size',
                        help="""files less than %(metavar)s bites
                              are put into the 'Small'""",
                        type=int,
                        required=True,
                        metavar="<size>",
                        default='4096')

    parsed = parser.parse_args()
    return parsed.source, parsed.days, parsed.size


def move_files_to_dir(*pairs):
    for dirname, filelist in pairs:
        if len(filelist) > 0:
            try:
                os.mkdir(dirname)
            except FileExistsError:
                pass
            for f in filelist:
                copyfile(f, os.path.join(dirname, f))

source, days, size = check_arg()

curdir = os.getcwd()

try:
    os.chdir(source)

    archive = list()
    small = list()

    range_date = time.time() - timedelta(days=days).total_seconds()

    files = [f for f in os.listdir() if os.path.isfile(f)]
    archive = [f for f in files if os.path.getmtime(f) < range_date]
    small = [f for f in files if os.path.getsize(f) < size]
    move_files_to_dir(("Archive", archive), ("Small", small))
    for f in set(archive + small):
        os.remove(f)

finally:
    os.chdir(curdir)
