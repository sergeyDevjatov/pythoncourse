import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from stringformatter_demo import Ui_StringFormatterDemo
from StringFormatter import StringFormatter


class MainForm(QMainWindow):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.ui = Ui_StringFormatterDemo()
        self.ui.setupUi(self)

    def format(self):
        formatter = StringFormatter(self.ui.string.text())
        if self.ui.removeWordsCheckbox.isChecked():
            formatter = formatter.delete_words(self.ui
                                               .minWordLengthSpin.value())
        if self.ui.replaceDigitsCheckbox.isChecked():
            formatter = formatter.digit_replace()
        if self.ui.sortCheckbox.isChecked():
            if self.ui.byAlphaRBtn.isChecked():
                formatter = formatter.sort_by_alpha()
            elif self.ui.byLengthRBtn.isChecked():
                formatter = formatter.sort_by_len()
        if self.ui.insertSpacesCheckbox.isChecked():
            formatter = formatter.space_insert()
        self.ui.result.setText(str(formatter))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = MainForm()
    form.show()
    try:
        sys.exit(app.exec_())
    except SystemExit as se:
        print('Program has exited with code {}'.format(se))
