from itertools import takewhile


class StringFormatter(object):
    def __init__(self, string, **kwargs):
        self.__original_string = string
        self.__separators = (kwargs['separators']
                             if 'separators' in kwargs else [' '])

    def __str__(self):
        return self.__original_string

    def __repr__(self):
        return "StringFormatter('{}')".format(self.__original_string)

    def __is_separator(self, string):
        return all(map(lambda c: c in self.__separators, string))

    def __split(self):
        words = list()
        separators = list()
        string = self.__original_string
        i = 0
        while True:
            word = ''.join(takewhile(lambda c: not self.__is_separator(c),
                                     string[i:]))
            i += len(word)
            words.append(word)
            separator = ''.join(takewhile(self.__is_separator, string[i:]))
            i += len(separator)
            separators.append(separator)
            if i >= len(string):
                break
        return zip(words, separators)

    def delete_words(self, min_word_len):
        return StringFormatter(''.join(word + sep
                                       for word, sep in self.__split()
                                       if not len(word) < min_word_len),
                               separators=self.__separators)

    def digit_replace(self):
        return StringFormatter(''.join('*' if char.isdigit() else char
                               for char in self.__original_string),
                               separators=self.__separators)

    def space_insert(self):
        return StringFormatter(' '.join(self.__original_string),
                               separators=self.__separators)

    def sort_by_alpha(self):
        return self.__sort(None)

    def sort_by_len(self):
        return self.__sort(len)

    def __sort(self, key):
        words, separators = list(zip(*self.__split()))
        return StringFormatter(''.join(word + sep
                                       for word, sep in zip(sorted(words,
                                                                   key=key),
                                                            separators)))
