def slice_to_equal_parts(arr, length):
    return zip(*[iter(arr)]*length)


def random_game_time():
    return random.choice((15, 18)), random.randrange(0, 45, 15)


def game_date():
    _14_days = datetime.timedelta(days=14)
    date = datetime.datetime(2016, 9, 14, *game_time())
    while True:
        yield date
        time = game_time()
        date += datetime.timedelta(
            hours=random.randint(3, 4),
            minutes=random.randrange(0, 45, 15))

        yield date
        time = random_game_time()
        date = date.replace(hour=time[0], minute=time[1]) + _14_days


def games_info(commands):
    if len(commands) != 16:
        raise ValueError("Игры проводятся только для набора из 16 команд.")
    alpha = "ABCD"
    groups = dict()
    random.shuffle(commands)
    groups = dict(zip(alpha, slice_to_equal_parts(commands, 4)))

    gd = game_date()
    for name, group in sorted(groups.items()):
        print("ГРУППА {}:".format(name))
        pairs = list(itertools.combinations(group, 2))
        while(len(pairs) > 0):
            firstPair, *pairs, secondPair = pairs
            for teamA, teamB in [firstPair, secondPair]:
                print("{} - {} [::] {}".format(teamA, teamB, next(gd).strftime("%d/%m/%y, %H:%M")))
            print()
        print()
    return groups

commands = [
    "Арсенал",
    "ПСЖ",
    "Лудогорец",
    "Базель",
    "Монако",
    "Байер 04",
    "Тоттенхэм",
    "ЦСКА",
    "Боруссия Д",
    "Реал Мадрид",
    "Легия",
    "Спортинг",
    "Лестер Сити",
    "Порту",
    "Копенгаген",
    "Брюгге"
    ]

grouped_commands = games_info(commands)
