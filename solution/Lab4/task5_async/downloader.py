import urllib.request as req
from urllib.parse import unquote
from os import path
from datetime import datetime
import concurrent
import asyncio


class downloader_async(object):
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)
    loop = asyncio.get_event_loop()
    _count = 0
    async def download(self, response, output, count):
        await type(self).loop.run_in_executor(type(self)
                                              .executor,
                                              lambda: output
                                              .write(response
                                                     .read(count)))

    def __init__(self, url, update_callback, finish_callback):
        self._url = url
        self._update_callback = update_callback
        self._finish_callback = finish_callback
        type(self)._count += 1

    async def __call__(self):
        self.filename = unquote(self._url).split('/')[-1]
        response = req.urlopen(self._url)
        count = 4096
        self.file_size = response.length
        with open(path.join('downloads', self.filename), 'wb') as output:
            start_time = datetime.now()
            while response.length:
                await self.download(response, output, count)
                self._update_callback(1 - response.length / self.file_size)
            self.timedelta = datetime.now() - start_time
            self._update_callback(1)
        type(self)._count -= 1
        if not type(self)._count:
            self._finish_callback()
