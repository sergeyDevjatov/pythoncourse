using System;

namespace Test{
	class Program{
		static int a(int m,int n){
			int r = m%n;
			if(r==0)
				return n;
			else
				return a(n,r);
		}

		static void Main(){
			int m,n;
			m = int.Parse(Console.ReadLine());
			n = int.Parse(Console.ReadLine());	

			Console.WriteLine(a(m,n));
		}
	}
}
