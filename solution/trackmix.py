import argparse
import os
import re
import subprocess as subpr
from random import randrange

FFMPEG = "ffmpeg"


def check_args():
    ''' Parsing args '''
    parser = argparse.ArgumentParser(
        description='Reorganize files in some directory.')

    parser.add_argument('-s', '--source',
                        help='name of work directory',
                        metavar="<path>",
                        required=True)
    parser.add_argument('-d', '--destination',
                        help='name of output file',
                        metavar="<dest>",
                        default='mix.mp3')
    parser.add_argument('-c', '--count',
                        help='count of processed files',
                        type=int,
                        metavar="<count>",
                        default=None)
    parser.add_argument('-f', '--frame',
                        help='duration of frame in seconds',
                        type=int,
                        metavar="<frame>",
                        default=10)
    parser.add_argument('-l', '--log',
                        help='print information during processing',
                        action="store_true")
    parser.add_argument('-e', '--extended',
                        help='add fade to frames',
                        action="store_true")

    parsed = parser.parse_args()
    return (parsed.source, parsed.destination,
            parsed.count, parsed.frame,
            parsed.log, parsed.extended)


def get_file_duration(file):
    fformat = subpr.run(["ffprobe", "-i", file, "-show_format"],
                        stdout=subpr.PIPE)
    if fformat.returncode != 0:
        raise RuntimeError("File cannot be read as a media!")
    dur, *_ = re.search("^duration=(.+?)$", fformat.stdout.decode(),
                        flags=re.M).groups()
    return int(dur.split(".")[0])


def cut_file():
    counter = 1

    def _(file, new_file, frame, **kwargs):
        nonlocal counter
        if kwargs['log']:
            print("--- processing file {}: '{}'".format(counter, file))
        dur = get_file_duration(file)
        ss = randrange(20, dur - frame - 20)
        fade_dur = max(min(5, frame//4), 1)

        if kwargs['fade']:
            af = ["-af", "afade=t=in:st={}:d={},afade=t=out:st={}:d={}"
                  .format(ss, fade_dur, ss + frame - fade_dur, fade_dur)]
        else:
            af = []

        out = subpr.run(["ffmpeg",
                         "-i", file,
                         "-ss", str(ss),
                         "-t", str(frame),
                         *af,
                         "-y",
                         new_file])
        if out.returncode != 0:
            raise RuntimeError("Failed cutting!")
        counter += 1
    return _


def concat(files, dest):
    out = subpr.run(["ffmpeg", "-y", "-i",
                     "concat:" + "|".join(cutted_files), dest])
    if out.returncode != 0:
        raise RuntimeError("Failed merging!")

source, dest, count, frame, isLog, isFade = check_args()

curdir = os.getcwd()

cut_file = cut_file()

try:
    os.chdir(source)
    files = os.listdir('.')
    cutted_files = list()
    try:
        os.mkdir(".cutted")
    except FileExistsError:
        pass
    for file in [f for f in files if f.endswith(".mp3")][:count]:
        name, ext = os.path.splitext(file)
        cutted_filename = ".cutted/{}_cutted{}".format(name, ext)
        try:
            cut_file(file, cutted_filename, frame, fade=isFade, log=isLog)
        except RuntimeError:
            if isLog:
                print("Processing failed!")
        else:
            cutted_files.append(cutted_filename)
        if isLog:
            print()

    if isLog:
        print("--- merge all files")
    try:
        concat(cutted_files, dest)
    except RuntimeError:
        if isLog:
            print("--- Failed")
    else:
        if isLog:
            print("--- Done")
    for f in cutted_files:
        os.remove(f)

finally:
    os.chdir(curdir)
